var form = layui.form
//-------------获取真实分类----------
$.ajax({
    url: '/my/category/list',
    success: function (res) {
        var html = template('tpl-category', res)
        $('select').html(html)
        form.render('select');
    }
})



// 初始化富文本编辑器
initEditor()

// 封面处理----------- 
var $image = $('#image')
var option = {
    // 宽高-
    aspectRatio: 400 / 280,
    preview: '.img-preview'
}

// 出现剪裁框
$image.cropper(option);

// 点击选择封面 选中图片..
$('button:contains("选择封面")').on('click', function () {
    $('#file').trigger('click')
})

// 更换图片 把剪裁区图片更换
$('#file').on('change', function () {
    if (this.files.length > 0) {
        // 找文件对象
        var fileObj = this.files[0];
        // 创建预览的url
        var url = URL.createObjectURL(fileObj);
        // 更换剪裁区的图片（销毁之前的剪裁区 --> 更换图片 --> 重新生成剪裁区）
        $image.cropper('destroy').attr('src', url).cropper(option);
    }
})





$('form').on('submit', function (e) {
    e.preventDefault()
    var data = new FormData(this)
    data.set('content', tinyMCE.activeEditor.getContent());
    var canvas = $image.cropper('getCroppedCanvas', {
        width: 400,
        height: 280
    });
    canvas.toBlob(function (blob) {
        // console.log(blob);
        data.append('cover_img', blob);

        $.ajax({
            type: 'POST',
            url: '/my/article/add',
            data: data,
            processData: false,
            contentType: false,
            success: function (res) {
                layer.msg(res.message);
                if (res.status === 0) {
                    location.href ='./list.html'
                }
            }
        })
    });
})