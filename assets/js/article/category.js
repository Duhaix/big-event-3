
var addIndex
$('button:contains("添加类别")').on('click', function () {
    addIndex = layer.open({
        type: 1,
        area: ['500px', '250px'],
        title: '添加分类',
        content: $('#tpl-add').html(),
    });
})



$('body').on('submit', '#add-form', function (e) {
    e.preventDefault()
    var data = $(this).serialize()

    $.ajax({
        type: 'POST',
        url: '/my/category/add',
        data: data,
        success: function (res) {
            layer.msg(res.message)
            if (res.status == 0) {
                layer.close(addIndex)
                fn()
            }
        }
    })

})




// --------------------获取分类---------------
function fn() {
    $.ajax({
        url: '/my/category/list',
        success: function (res) {
            if (res.status === 0) {
                var html = template('box', res)
                $('tbody').html(html)
            }
        }
    })
}
fn()



// ------------------删除分类-----------------
$('tbody').on('click', 'button:contains("删除")', function () {
    let id = $(this).data("id");
    layer.confirm('是否要删除?', { icon: 3, title: '提示' }, function (index) {
        //do something
        $.ajax({
            url: '/my/category/delete',
            data: { id: id },
            success: function (res) {
                layer.msg(res.message)
                fn()
            }
        })
    });
})



// ------------------编辑分类--------------
var UserIndex
$('tbody').on('click', 'button:contains("编辑")', function () {
    var data = $(this).data()
    UserIndex = layer.open({
        type: 1,
        area: ['500px', '250px'],
        title: '编辑分类',
        content: $('#tpl-user').html(),
        // ----回填-----
        success: function () {
            var form = layui.form
            form.val("user", data)
        }
    });
})


$('body').on('submit', '#user-form', function (e) {
    e.preventDefault()
    var data = $(this).serialize()
    console.log(data);
    $.ajax({
        type: 'POST',
        url: '/my/category/update',
        data:data,
        success: function (res) {
            layer.msg(res.message)
            if (res.status == 0) {
                fn()
                layer.close(UserIndex)
            }
            
        }
    })
})