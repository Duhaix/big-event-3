var form = layui.form

form.verify({
    //我们既支持上述函数式的方式，也支持下述数组的形式
    //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
    pass: [
        /^[\S]{6,12}$/
        , '密码必须6到12位，且不能出现空格'
    ],
    diff: function (val) {
        var oldPwd = $('input[name=oldPwd]').val()
        if (val === oldPwd) {
            return '新密码不能和原密码相同'
        }
    },
    some: function (val) {
        var newPwd = $('input[name=newPwd]').val()
        if (val !== newPwd) {
            return '两次密码不一致'
        }
    }
});


$('form').on('submit', function (e) {
    e.preventDefault();
    var data = $(this).serialize();
    $.ajax({
        type: 'POST',
        url: '/my/user/updatepwd',
        data: data,
        success: function (res) {
            layer.msg(res.message)
            if (res.status === 0) {
                window.parent.location.href ='../login.html'
            }
        }
    })
})