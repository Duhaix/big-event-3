var form = layui.form
console.log(form);
function renderUser() {
    $.ajax({
        url: '/my/user/userinfo',
        success: function (res) {
            console.log(res);
            if (res.status === 0) {
                form.val('user', res.data)
            }
        }
    })
}
renderUser()


console.log(1);
$('form').on('submit', function (e) {
    e.preventDefault()
    var data = $(this).serialize()

    $.ajax({

        type: 'POST',
        url: '/my/user/userinfo',
        data: data,
        success: function (res) {
            layer.msg(res.message);
            if (res.status === 0) {
                window.parent.getUserInfo();
            }
        }
    })
})

$('button[type=reset]').on('click', function (e) {
    e.preventDefault()
    renderUser()
})