// 1.1 获取裁剪区域的 DOM 元素
var $image = $('#image')



// 1.2 配置选项
const options = {
    // 纵横比
    aspectRatio: 1,
    // 指定预览区域
    preview: '.img-preview'
}

// 1.3 创建裁剪区域
$image.cropper(options);

// 选择包含有 上传 俩字的摁扭
$('button:contains("上传")').on('click', function () {
    $('#file').trigger('click');
})
$('#file').on('change', function () {
    if (this.files.length > 0) {
        var fileObj = this.files[0]
        var url = URL.createObjectURL(fileObj)
        $image.cropper('destroy').attr('src', url).cropper(options)
    }
})



$('button:contains("确定")').on('click', function () {
    var canvas = $image.cropper('getCroppedCanvas', { // 创建一个 Canvas 画布
        width: 100,
        height: 100
    }).toDataURL('image/png')       // 将 Canvas 画布上的内容，转化为 base64 格式的字符串
    // console.log(canvas);

    $.ajax({
        type: 'POST',
        url: '/my/user/avatar',
        data: { avatar: canvas },
        success: function (res) {
            layer.msg(res.message)
            if (res.status == 0) {
                window.parent.getUserInfo();
            }
        }
    })

})


